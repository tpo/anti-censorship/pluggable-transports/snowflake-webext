/*
Only WebSocket-specific stuff.
*/

// eslint-disable-next-line no-unused-vars
class WS {
  /**
   * Creates a WebSocket URL from a base URL and an optional client IP address
   * string.
   * @param {URL|string} url
   * @param {?string} clientIP
   * @return {URL}
   */
  static makeWebSocketURL(url, clientIP) {
    url = new URL(url);
    if (clientIP != null) {
      url.searchParams.set('client_ip', clientIP);
    }
    return url;
  }

  /**
   * Creates a WebSocket connection from a URL and an optional client IP address
   * string.
   * @param {URL|string} url
   * @param {?string} clientIP
   * @return {WebSocket}
   */
  static makeWebSocket(url, clientIP) {
    let ws = new WebSocket(WS.makeWebSocketURL(url, clientIP));
    /*
    'User agents can use this as a hint for how to handle incoming binary data:
    if the attribute is set to 'blob', it is safe to spool it to disk, and if it
    is set to 'arraybuffer', it is likely more efficient to keep the data in
    memory.'
    */
    ws.binaryType = 'arraybuffer';
    return ws;
  }

  /**
   * @param {URL | string} addr
   */
  static probeWebSocket(addr) {
    return /** @type {Promise<void>} */(new Promise((resolve, reject) => {
      const ws = WS.makeWebSocket(addr, null);
      ws.onopen = () => {
        resolve();
        ws.close();
      };
      ws.onerror = () => {
        reject();
        ws.close();
      };
    }));
  }

}
