describe('makeWebSocketURL', function() {
  it('attaches client_ip', function() {
    // With no clientIP, should leave query parameters unchanged.
    expect(WS.makeWebSocketURL('wss://example.com/path', null).toString())
      .toBe('wss://example.com/path');
    expect(WS.makeWebSocketURL('wss://example.com/path?client_ip=192.0.2.99', null).toString())
      .toBe('wss://example.com/path?client_ip=192.0.2.99');
    expect(WS.makeWebSocketURL('wss://example.com/path?client_ip=192.0.2.98&client_ip=192.0.2.99', null).toString())
      .toBe('wss://example.com/path?client_ip=192.0.2.98&client_ip=192.0.2.99');

    // Should add a client_ip query parameter.
    expect(WS.makeWebSocketURL('wss://example.com/path', '192.0.2.1').toString())
      .toBe('wss://example.com/path?client_ip=192.0.2.1');

    // Should retain any other existing query parameters.
    expect(WS.makeWebSocketURL('wss://example.com/path?foo=bar', '192.0.2.1').toString())
      .toBe('wss://example.com/path?foo=bar&client_ip=192.0.2.1');

    // Should overwrite any existing client_ip query parameters.
    expect(WS.makeWebSocketURL('wss://example.com/path?client_ip=192.0.2.99', '192.0.2.1').toString())
      .toBe('wss://example.com/path?client_ip=192.0.2.1');
    expect(WS.makeWebSocketURL('wss://example.com/path?client_ip=192.0.2.98&client_ip=192.0.2.99', '192.0.2.1').toString())
      .toBe('wss://example.com/path?client_ip=192.0.2.1');
  });
});
